from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
def configure_iftpsi_selection() :
  from os import environ
  from Configurables import GaudiSequencer, CombineParticles
  from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS, FilterDesktop, MCDecayTreeTuple, TupleToolPrimaries, TupleToolStripping
  from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln, TupleToolPid, EventCountHisto, TupleToolRecoStats
  from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo
  from Configurables import LoKi__Hybrid__TupleTool, TupleToolVeto
  from Configurables import LoKi__Hybrid__EvtTupleTool
  # add xml catalog that maps the LFN and GUID of a file to its PFN
  from Gaudi.Configuration import FileCatalog
  FileCatalog().Catalogs += [ 'xmlcatalog_file:pool_xml_catalog.xml' ]

  from PhysSelPython.Wrappers import AutomaticData, SelectionSequence, Selection, MergedSelection
  import sys

 # --------------------------------------
 # Lists of trigger and stripping lines
 # --------------------------------------
  mtl= ["L0MUONDecision",
      "L0SPDDecision",
      "L0PUDecision",
      "L0SPDLowMultDecision",
      "L0CALODecision",
      "Hlt1DiMuonHighMassDecision",
      "Hlt1BBMicroBiasVeloDecision",
      "Hlt1LumiLowBeamCrossingDecision",
      "Hlt1VeloClosingMicroBiasDecision",
      "Hlt2BBPassThroughDecision"
      ]
  mstrip = ["StrippingHeavyIonTopologyLowActivityLineDecision"]
  
  # ----------------------------
  # Selection
  # ----------------------------
  sel={}
  sel["Muons"] = AutomaticData(Location = "Phys/StdAllLooseMuons/Particles")
  #combine particles
  Jpsi2MuMu = CombineParticles("Jpsi2MuMu")
  Jpsi2MuMu.DecayDescriptor = "J/psi(1S) -> mu+ mu-"
  #Jpsi2MuMu.DaughtersCuts = {
	  #'' : 'ALL',
	  #'mu+' : 'ALL',
	  #'mu-' : 'ALL',
	  #'mu+' : '( PT > 400 * MeV ) & ( TRCHI2DOF < 4 )' ,
	  #'mu-' : '( PT > 400 * MeV ) & ( TRCHI2DOF < 4 )' ,
	  #}
  Jpsi2MuMu.CombinationCut = "(ADAMASS ( 'J/psi(1S)' ) < 2500 * MeV)"
  Jpsi2MuMu.MotherCut = "(VFASPF(VCHI2/VDOF) < 10)"
  # make a selection
  sel["Jpsi2MuMu_sel"] = Selection('SelJpsi2MuMu', Algorithm=Jpsi2MuMu, RequiredSelections=[sel["Muons"]])
  # build the selection sequence
  seq={}
  seqList=[] # to put in the sequence
  for s in sel.keys():
    seq[s] = SelectionSequence("seq_"+s, TopSelection = sel[s] );
    seqList += [seq[s].sequence()]
    
  # -------------------------------
  # Settings for HERSCHEL tool
  # -------------------------------
  from Configurables import DaVinci, CondDB
  CondDB( LatestGlobalTagByDataType = "2015" )
  # Add decoder tool for HERSCHEL raw-banks
  from Configurables import HCRawBankDecoder
  decoder = HCRawBankDecoder()
  
  # -------------------------------
  # Make tuple
  # -------------------------------
  tuple_Jpsi = DecayTreeTuple("tuple_Jpsi")
  tuple_Jpsi.Inputs = [seq["Jpsi2MuMu_sel"].outputLocation()]
  tuple_Jpsi.Decay = "J/psi(1S) -> ^mu+ ^mu-"
  branches_Jpsi = {
	  "J_psi_1S":"J/psi(1S) -> mu+ mu-",
	  "muplus":"J/psi(1S) -> ^mu+ mu-",
	  "muminus":"J/psi(1S) -> mu+ ^mu-",
	  }
  tuple_Jpsi.addBranches(branches_Jpsi)

  tl= [ "TupleToolKinematic", "TupleToolPid","TupleToolPrimaries", "TupleToolEventInfo","TupleToolRecoStats","TupleToolTISTOS","TupleToolGeometry"]
  tuple_Jpsi.ToolList += tl

  track_tool = tuple_Jpsi.addTupleTool('TupleToolTrackInfo')
  track_tool.Verbose = True

  allVeloTr_tool = tuple_Jpsi.addTupleTool('TupleToolAllVeloTracks')
  allVeloTr_tool.Verbose = True

  herschel_tool = tuple_Jpsi.addTupleTool('TupleToolHerschel')
  herschel_tool.DigitsLocation="/Event/Raw/HC/Digits"

  trigger_tool_Jpsi = tuple_Jpsi.J_psi_1S.addTupleTool('TupleToolTISTOS')
  trigger_tool_Jpsi.Verbose=True
  trigger_tool_Jpsi.TriggerList = mtl

  tuple_Jpsi.ToolList += ["TupleToolTISTOS"]
  tuple_Jpsi.addTool(TupleToolTISTOS, name = "TupleToolTISTOS")
  tuple_Jpsi.TupleToolTISTOS.Verbose = True
  tuple_Jpsi.TupleToolTISTOS.TriggerList = mtl
  
  # -----------------------
  # LoKi variables
  # -----------------------
  loki_Jpsi = tuple_Jpsi.J_psi_1S.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_EvtTuple_Jpsi')
  loki_Jpsi.Variables = {
	  "ETA": "ETA",
	  "Y"  : "Y",
	  "PHI"  : "PHI",
	#"LOKI_BPVVDCHI2" : "BPVVDCHI2",# bestVertex error
	#"LOKI_BPVDLS"    : "BPVDLS",	# bestVertex error
	"DOCA"       	 : "DOCA(1,2)",
	"LOKI_VCHI2VDOF" : "VFASPF(VCHI2/VDOF)"
	  }

  #LoKi muplus
  loki_mup = tuple_Jpsi.muplus.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_EvtTuple_mup')
  loki_mup.Variables = {
	  "ETA": "ETA",
	  "Y"  : "Y",
	  "PHI"  : "PHI",
	  "KL" : "CLONEDIST"
	  }

  #LoKi muminus
  loki_mum = tuple_Jpsi.muminus.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_EvtTuple_mum')
  loki_mum.Variables = {
	  "ETA": "ETA",
	  "Y"  : "Y",
	  "PHI"  : "PHI",
	  "KL" : "CLONEDIST"
	  }
  tuple_Jpsi.addTupleTool(TupleToolStripping, name="TupleToolStripping")
  tuple_Jpsi.TupleToolStripping.StrippingList = mstrip

  # ================================
  # Make the tuple list
  # ================================
  tupleList=[]
  tupleList+=[tuple_Jpsi]
  # ================================
  # Track scale for data
  # ================================
  from Configurables import TrackScaleState as SCALER
  scaler=SCALER('Scaler')

  return [ scaler ] + seqList + [ decoder ] + tupleList
