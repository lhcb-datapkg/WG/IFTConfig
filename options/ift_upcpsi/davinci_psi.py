# ================================
# Read only low-mult events
# ================================

#from Configurables import LoKi__VoidFilter
#fltrs = LoKi__VoidFilter( Code = "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 20) & ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 2000 ) & ( recSummaryTrack(LHCb.RecSummary.nVeloTracks,TrVELO) < 11 )" , 
#                          Preambulo = ["from LoKiTracks.decorators import *" ,
#                                       "from LoKiCore.functions    import * ",
#                                       "from GaudiKernel.SystemOfUnits import *"] )

#from PhysConf.Filters import LoKi_Filters
#fltrs = LoKi_Filters (
  #VOID_Code = "( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) > 0) & ( recSummaryTrack(LHCb.RecSummary.nVeloTracks,TrVELO) < 7 )" ,
  #VOID_Preambulo = ["from LoKiTracks.decorators import *" ,
                    #"from LoKiCore.functions    import * ",
                    #"from GaudiKernel.SystemOfUnits import *"] )

from Configurables import LoKi__HDRFilter 
fltrs = LoKi__HDRFilter( 'StripPassFilter', 
			Code="HLT_PASS('StrippingHeavyIonTopologyLowActivityLineDecision')", 
			Location="/Event/Strip/Phys/DecReports" )
# ================================
# Configure DaVinci
# ================================

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci

davinci = DaVinci (
    EvtMax          =     -1    ,
    SkipEvents      =      0	,
    PrintFreq       =  10000    ,
    DataType        = "2015"	,
    DDDBtag         = ""	,	# Default as set in DDDBConf for DataType
    CondDBtag       = ""	,	# Default as set in DDDBConf for DataType
    DQFLAGStag      = ""	,	# Default as set in DDDBConf for DataType
    #Input           = []	,
    EventPreFilters = fltrs.filters ('fltrs'),
    InputType       = "DST"	,
    TupleFile       = "IFT_PSI.ROOT"	,
    #UserAlgorithms  = [SeqPhys]	,
    Lumi            =   True
    )