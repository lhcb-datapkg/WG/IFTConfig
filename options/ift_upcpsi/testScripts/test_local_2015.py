from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
     'PFN:root://f01-080-123-e.gridka.de:1094/pnfs/gridka.de/lhcb/buffer/lhcb/LHCb/Lead15/RDST/00050695/0000/00050695_00000865_1.rdst'] )

# ================================
# Read only low-mult events
# ================================

#from Configurables import LoKi__VoidFilter
#fltrs = LoKi__VoidFilter( Code = "( recSummaryTrack(LHCb.RecSummary.nLongTracks, TrLONG) < 20) & ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 2000 ) & ( recSummaryTrack(LHCb.RecSummary.nVeloTracks,TrVELO) < 11 )" , 
#                          Preambulo = ["from LoKiTracks.decorators import *" ,
#                                       "from LoKiCore.functions    import * ",
#                                       "from GaudiKernel.SystemOfUnits import *"] )

from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
  VOID_Code = "( recSummaryTrack(LHCb.RecSummary.nVeloTracks, TrVELO) > 0) & ( recSummaryTrack(LHCb.RecSummary.nVeloTracks,TrVELO) < 7 )" ,
  VOID_Preambulo = ["from LoKiTracks.decorators import *" ,
                    "from LoKiCore.functions    import * ",
                    "from GaudiKernel.SystemOfUnits import *"] )


# ================================
# Configure DaVinci
# ================================

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci

davinci = DaVinci (
    EventPreFilters = fltrs.filters ('Filters') ,
    EvtMax          =     -1    ,
    PrintFreq       =   1000    ,
    Lumi            =   True    ,
    TupleFile       = "IFT_PSI.ROOT"
    )

# Helper file to define data type for davinci
the_year = '2015'
the_fileType  = 'RDST'

from Configurables import DaVinci, CondDB
dv = DaVinci ( DataType                  = the_year           ,
               InputType                 = the_fileType       ,
             )

from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
from ift_psi.selectpsi import configure_iftpsi_selection
dv.appendToMainSequence( configure_iftpsi_selection( dataYear = the_year ) )

db = CondDB  ( LatestGlobalTagByDataType = the_year )

if '__main__' == __name__ :

  print ' The year : ', the_year

