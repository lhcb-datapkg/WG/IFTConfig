
# Helper file to define data type for davinci

the_year      = '2015'
the_fileType  = 'DST'

from Configurables import DaVinci, CondDB
dv = DaVinci ( DataType                  = the_year           ,
               InputType                 = the_fileType       ,
             )

from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
from ift_upcpsi.selectpsi import configure_iftpsi_selection
dv.appendToMainSequence( configure_iftpsi_selection() )

db = CondDB  ( LatestGlobalTagByDataType = the_year )

if '__main__' == __name__ :

  print ' The year : ', the_year

